const inputTime = document.getElementById("inputTime");
const inputDate = document.getElementById("inputDate");
const inputTimeZone = document.getElementById("inputTimeZone");
const outputTimeZone = document.getElementById("outputTimeZone");

function timeWasInputted(event) {
    let date = inputDate.value;
    let time = inputTime.value;
    let inputZone = inputTimeZone.value;
    let outputZone = outputTimeZone.value;

    event.preventDefault();

    let inputDateTimeString = date + "T" + time;
    let inputDateTime = new Date(inputDateTimeString);
    let inputTimestamp = inputDateTime.getTime();
    let outputDateTime = new Date(inputTimestamp);
    let output = outputDateTime.toLocaleString("en-US", {
        timeZone: outputZone,
        hour12: true
    });
    result.textContent = `The time in ${outputTimeZone.selectedOptions[0].id} is ${output}`;
}

const submitButton = document.getElementById("enter");
const log = document.getElementById("log");
const result = document.getElementById("result");

submitButton.addEventListener("click", timeWasInputted);
